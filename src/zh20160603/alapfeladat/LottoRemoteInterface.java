package zh20160603.alapfeladat;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Set;

/**
 * Created by atesztoth on 2017. 05. 10..
 */
public interface LottoRemoteInterface extends Remote {

    public void sorsol() throws RemoteException;
    public int jatszik(Set<Integer> szamok) throws RemoteException;

}
