package zh20160603.alapfeladat;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;
import java.util.Set;

/**
 * Created by atesztoth on 2017. 05. 10..
 */

// https://docs.oracle.com/javase/8/docs/technotes/guides/rmi/hello/hello-world.html
public class Lottozo implements LottoRemoteInterface {

    private Random random;
    private final int MAX = 90;
    private final int DB = 5;
    private Set<Integer> winningNumbers;
    private int depot;

    public static void main(String[] args) {
        // 1. Create remote object
        int depot = Integer.valueOf(args[0]);
        Lottozo lottozo = new Lottozo(depot);

        try {
            // 2. unicast it:
            LottoRemoteInterface stub = (LottoRemoteInterface) UnicastRemoteObject.exportObject(lottozo, 8899);

            // Bind the remote object's stub in the registry:
            Registry registry = LocateRegistry.getRegistry(); // getting the registry object
            // Binding the stub to a particular string.
            registry.bind("lottozo", stub);

            System.out.println("Server ready!");
        } catch (RemoteException e) {
            System.out.println(e.toString());
            e.printStackTrace();
        } catch (AlreadyBoundException e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }

    }

    public Lottozo(int depot) {
        this.random = new Random(123456);
        depot = depot;
    }

    @Override
    public synchronized void sorsol() throws RemoteException {
        if (depot < 0) throw new UnsupportedOperationException("lottoz is banktrupt!");

        winningNumbers.clear();

        while (winningNumbers.size() < 90) {
            winningNumbers.add(random.nextInt(MAX) + 1);
        }

        System.out.println("L > sorsolas: " + winningNumbers);
    }


    @Override
    public int jatszik(Set<Integer> szamok) {
        int reward = 0;
        int price = 2;

        return reward;
    }
}
