/**
 * Created by atesztoth on 2017. 03. 08..
 */
public class Parallel {

    // static int n = 100; // Not so pretty, violates publicity priciples.

    public static void main(String[] args) {
        for (int i = 0; i < 50; ++i) {
            new Szal().start();
        }
    }

//    static class A {
//        // Nesting classes
//    }

}

class Szal extends Thread {

    private static int n = 100;
    private static Object lock = new Object();

    @Override
    public void run() {
        String s = "Hello word";
        while (true) { // It is not a good idea to sync whole while, cuz then
            // only one thread is gonna --n, till it reaches 0.
            // BUT THIS WAY: every thread can minus it.
            // synchronized (Szal.class) {
            synchronized (lock) { // read after it! both could work, but this makes some magic
                if (n > 0) {
                    --n;
                    System.out.println(n + " " + this.getName());
                } else {
                    break;
                }
            }
        }
    }
}