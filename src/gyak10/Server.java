package gyak10;

import ServerPackage.AbstractServer;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by atesztoth on 2017. 05. 03..
 */
public class Server extends AbstractServer {

    public static void main(String[] args) {
        Server server = new Server(65510);

        try {
            server.acceptConnections();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Server(int port) {
        super(port);
    }

    /**
     * This method accepts as many connections as defined by num.
     */
    public void acceptConnections() throws IOException {

        int numberOfConnections = 0;

        while (true) {
            // waiting for connections forever.
            ArrayList<ServerClientHelper> clients = new ArrayList<>();

            Socket client = serverSocket.accept();
            ++numberOfConnections;

            ServerClientHelper helper = new ServerClientHelper();
            // Setting up read and write points:
            helper.setInputStream(client.getInputStream());
            helper.setOutputStream(client.getOutputStream());
            // Make the object setup itself:
            helper.setup();

            // Ask for the reader reference, and store the value:
            helper.setName(helper.getScanner().nextLine());

            clients.add(helper);

            if(3 == numberOfConnections) {
                // Starting a new thread, and resetting counter
                numberOfConnections = 0;

                new ServerGameHandler(clients);
            }
        }
    }

}
