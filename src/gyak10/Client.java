package gyak10;

import com.sun.javaws.exceptions.InvalidArgumentException;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by atesztoth on 2017. 05. 03..
 */
public class Client {

    private String myName;

    private Socket socket;
    private PrintWriter printWriter;
    private Scanner scanner;

    public static void main(String[] args) throws InvalidArgumentException {
        if (1 != args.length) {
            String[] errors = new String[]{"Hibás számú argumentum."};
            throw new InvalidArgumentException(errors);
        }

        try {
            Client client = new Client(args[0]);
            client.startListening();
            client.stopListening();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Client(String name) throws IOException {
        myName = name;

        // Set up other objects:
        socket = new Socket("localhost", 65510);
        printWriter = new PrintWriter(socket.getOutputStream(), true);
        scanner = new Scanner(socket.getInputStream());
    }

    public void startListening() {
        String command = "";
        boolean run = true;

        while (run) {
            command = scanner.nextLine();

            run = handleServerCall(command);
        }
    }

    /**
     * ... adn also close the client
     */
    public void stopListening() throws IOException {
        socket.close();
    }

    /**
     * This method handles server's msg, when it returns false, while loop stops.
     *
     * @param call: Message of the server.
     * @return decision if run should be stopped.
     */
    private boolean handleServerCall(String call) {
        int num = 0;

        try {
            num = Integer.valueOf(call);
            num -= 1;

            // Handling loosing scenario:
            if(num < 0 ) {
                System.out.println("kiestem");
            }

            printWriter.println(num);
        } catch (Exception e) {
            System.out.printf("Nem sikerült a konverzió számmá.");
            e.printStackTrace();
        }

        return (num > 0);
    }
}
