package hatodik;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Created by atesztoth on 2017. 03. 29..
 */
public class NumberReader {

    private String fileName;
    private int intNum = 0;
    private int doubleNum = 0;
    private int sumInt = 0;
    private double sumDouble = 0.0;

    public static void main(String[] args) {
        NumberReader nr = new NumberReader("numbers.ser");

        nr.doRead();

        // stats:
        System.out.println("Int numbers: " + nr.getIntNum());
        System.out.println("Double numbers: " + nr.getDoubleNum());

        System.out.println("Int sum: " + nr.getSumInt());
        System.out.println("Double sum: " + nr.getSumDouble());
    }

    public NumberReader(String file) {
        this.fileName = file;
    }

    public int getIntNum() {
        return intNum;
    }

    public void setIntNum(int intNum) {
        this.intNum = intNum;
    }

    public int getDoubleNum() {
        return doubleNum;
    }

    public void setDoubleNum(int doubleNum) {
        this.doubleNum = doubleNum;
    }

    public int getSumInt() {
        return sumInt;
    }

    public void setSumInt(int sumInt) {
        this.sumInt = sumInt;
    }

    public double getSumDouble() {
        return sumDouble;
    }

    public void setSumDouble(double sumDouble) {
        this.sumDouble = sumDouble;
    }

    private void doRead() {
        // The file contains a serialized object so:
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));

            for (int i = 0; i < 100; i++) {
                Object obj = ois.readObject();
                if (obj instanceof Integer) {
                    ++intNum;

                    sumInt += (int) obj;
                } else if (obj instanceof Double) {
                    ++doubleNum;

                    sumDouble += (double) obj;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
