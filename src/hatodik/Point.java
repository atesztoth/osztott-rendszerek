package hatodik;

import java.io.Serializable;

/**
 * Created by atesztoth on 2017. 03. 29..
 */
public class Point implements Serializable{

    private int x;
    private int y;

    public static void main(String[] args) {
        Point p = Point.createNewPoint(5,2);

        p.move(1,1);
        System.out.println(p.toString());
    }

    private Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Point factory.
     *
     * @param x
     * @param y
     * @return
     */
    public static Point createNewPoint(int x, int y) {
        return new Point(x, y);
    }

    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }

    @Override
    public String toString() {
        return "(" + this.x + "," + this.y + ")";
    }

}
