package hatodik;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Random;

/**
 * Created by atesztoth on 2017. 03. 29..
 */
public class NumberWriter {

    public static void main(String[] args) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("numbers.ser")); // ser: serializált doc
        Random rand = new Random();

        int n;
        for (int i = 0; i < 100; i++) {
            n = rand.nextInt(2);
            Object o;

            if (0 == n) {
                o = rand.nextInt();
            } else {
                o = rand.nextDouble();
            }

            oos.writeObject(o);
        }

        oos.close();
    }

}
