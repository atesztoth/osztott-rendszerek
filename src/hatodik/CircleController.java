package hatodik;

import java.io.*;

/**
 * Created by atesztoth on 2017. 03. 29..
 */
public class CircleController {

    public static void main(String[] args) {
        CircleController cc = new CircleController();

        cc.thirdTaskAction();
    }

    private CircleController() {

    }

    private void thirdTaskAction() {
        Circle c = Circle.createCricleFromCenter(0, 0, 3.5);
        Circle c2 = Circle.createCricleFromCenter(1, 1, 4.2);

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("circle.ser"))) {
            oos.writeObject(c);
            oos.writeObject(c2);

            c2.move(1, 2);

            oos.reset(); // it caches

            oos.writeObject(c2);

            oos.close();

            thirdTaskRead();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void thirdTaskRead() {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("circle.ser"));

            Object obj1 = ois.readObject();
            Object obj2 = ois.readObject();
            Object obj3_moved = ois.readObject();

            System.out.println(obj1);
            System.out.println(obj2);
            System.out.println(obj3_moved);

            ois.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
