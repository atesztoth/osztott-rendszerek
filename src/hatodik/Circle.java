package hatodik;

import java.io.Serializable;

/**
 * Created by atesztoth on 2017. 03. 29..
 */
public class Circle implements Serializable {

    private Point o;
    private double radius;
    private double girth;
    private boolean isGirthCalcualted = false;

    public static void main(String[] args) {
        Point center = Point.createNewPoint(0, 0);
        Circle c = new Circle(center, 3.0);

        System.out.printf(c.toString());
    }

    public Circle(Point o, double radius) {
        this.o = o;
        this.radius = radius;
    }

    public static Circle createCricleFromCenter(int x, int y, double radius) {
        return new Circle((Point.createNewPoint(x, y)), radius);
    }

    public void move(int dx, int dy) {
        o.move(dx, dy);
    }

    public double getC() {
        calculateGirth();

        return this.girth;
    }

    private void calculateGirth() {
        if (isGirthCalcualted) {
            return;
        }
        System.out.println("// Debug: Girth calculated.");

        this.girth = Math.PI * 2 * this.radius;
        this.isGirthCalcualted = true;
    }

    @Override
    public String toString() {
        // <középpont>,r: <sugár>, c: <kerület>

        return "Középpont: " + o.toString() + ", r: " + radius + ", c: " + this.getC();
    }

}
