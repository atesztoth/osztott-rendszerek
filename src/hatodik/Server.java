package hatodik;

import ServerPackage.AbstractServer;

import java.io.IOException;

/**
 * Created by atesztoth on 2017. 03. 29..
 */
public class Server extends AbstractServer {

    public static void main(String[] args) {
        Server server = new Server(11223);

        try {
            server.createServer();


        } catch (IOException e) {
            System.out.println("Sikertelen server létrehozás!");
            e.printStackTrace();
        }


        try {
            server.stopServer();
        } catch (IOException e) {
            System.out.println("Nem tudtam leállítani a servert.");
            e.printStackTrace();
        }
    }

    public Server(int port) {
        super(port);
    }
}
