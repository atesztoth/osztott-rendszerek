package Hetedik;

import hatodik.Circle;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by atesztoth on 2017. 04. 05..
 */
public class RmiServer extends UnicastRemoteObject implements CircleRemote, ProcessString {

    // 7 / 1

    public static void main(String[] args) {
        RmiServer rmiserver = null;
        Registry registry = null;

        try {
            rmiserver = new RmiServer();
            registry = LocateRegistry.createRegistry(1099);
            registry.rebind("myserver", rmiserver);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public RmiServer() throws RemoteException {
        super();
        System.out.println("Létrejött a szerver!");
    }

    @Override
    public double getC(Circle c) throws RemoteException {
        return 0;
    }

    @Override
    public String process(String s) {
//        String returnString = "";
//
//        for (int i = s.length(); i >= 0; --i) {
//            returnString += s.charAt(i);
//        }
//
//        return returnString;

        return new StringBuilder(s).reverse().toString(); // Ok... You've just divided my mind by 0.
    }
}
