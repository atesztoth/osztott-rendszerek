package Hetedik;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by atesztoth on 2017. 04. 05..
 */
public interface ProcessString extends Remote{

    public String process(String s) throws RemoteException;

}
