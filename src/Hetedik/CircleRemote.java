package Hetedik;

import hatodik.Circle;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by atesztoth on 2017. 04. 05..
 */
public interface CircleRemote extends Remote {

    public double getC(Circle c) throws RemoteException;

}
