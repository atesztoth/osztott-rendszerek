package Hetedik;

import hatodik.Circle;
import hatodik.Point;

import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by atesztoth on 2017. 04. 05..
 * So this RMI thing uses java's registry for data exchange. This is the point.
 */
public class RmiClient {

    public static void main(String[] args) {
        Registry registry = null;
        Remote remote = null;

        try {
            registry = LocateRegistry.getRegistry();
            remote = registry.lookup("myserver");

            CircleRemote cr = (CircleRemote) remote;
            ProcessString ps = (ProcessString) remote; // So the remote can be anything it implements.

            Circle circle = new Circle(Point.createNewPoint(1, 2), 3);

            double kerulet = cr.getC(circle);
            System.out.println("Kerület: " + kerulet);

            // 7/2
            String fafString = ps.process("Totalteszt");
            System.out.println(fafString);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }

    }

}
