package ServerPackage;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by atesztoth on 2017. 03. 29..
 */
abstract public class AbstractServer implements ServerInterface {

    protected int port = 0;
    protected ServerSocket serverSocket;

    public AbstractServer(int port) {
        this.port = port;
    }

    @Override
    public void createServer() throws IOException {
        serverSocket = new ServerSocket(port);
    }

    @Override
    public void stopServer() throws IOException {
        serverSocket.close();
    }
}
