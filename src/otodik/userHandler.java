package otodik;

import java.util.Random;
import java.util.logging.Handler;

/**
 * Created by atesztoth on 2017. 03. 22..
 */
public class userHandler extends Thread {

    private Bankszamla bankszamla;

    public userHandler(Bankszamla bankszamla) {
        this.bankszamla = bankszamla;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            int actionCode = (new Random()).nextInt(2);
            int randomAmount = (new Random()).nextInt(5001);

            synchronized (Handler.class) {
                System.out.println(this.getName() + " (amount: " + randomAmount + ") action: ");
                if (actionCode < 1) {
                    // This case withdraw some money
                    bankszamla.withdraw(randomAmount);
                } else {
                    // This case add some
                    bankszamla.addAmount(randomAmount);
                }
            }

            try {
                Thread.sleep((new Random()).nextInt(3001));
            } catch (InterruptedException e) {
                System.out.printf("Nem tudott aludni lol.");
                e.printStackTrace();
            }
        }
    }

}
