package otodik;

import java.util.ArrayList;

/**
 * Created by atesztoth on 2017. 03. 22..
 */
public class Bankszamla {

    private int balance = 0;
    private int users = 0;
    private ArrayList<Thread> threadList = new ArrayList<>();

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            throw new Exception("Két paramétert várok!");
        }

        Bankszamla bankszamla = new Bankszamla(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        bankszamla.startSimulation();
    }

    public Bankszamla(int balance, int users) {
        this.balance = balance;
        this.users = users;
    }

    public int getBalance() {
        return this.balance;
    }

    public void startSimulation() throws InterruptedException {
        for (int i = 0; i < users; i++) {
            // attaching handlers to each user
            userHandler userHandler = new userHandler(this);
            threadList.add(userHandler);
            userHandler.start();
        }

        // Join waits for a thread to finish.
        for (Thread t : threadList) {
            t.join();

        }

        System.out.println("A program futása befejeződött.");
    }

    public void addAmount(int money) {
        System.out.println("Pénz befizetés. Régi egyenleg: " + this.balance + " új egyenleg: " + (this.balance += money));
    }

    public void withdraw(int money) {
        // balance can never be < 0
        System.out.print("Pénzfelvétel. Régi egyenleg: " + this.balance + " új egyenleg: ");
        if (money >= balance) {
            this.balance = 0;
        } else {
            this.balance -= money;
        }

        System.out.println(this.balance);
    }
}
