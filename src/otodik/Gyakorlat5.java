package otodik;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by atesztoth on 2017. 03. 22..
 */
public class Gyakorlat5 {

    public static void main(String[] args) throws InterruptedException {

    }

    private void masodikFeladat() {

    }

    private void elsoFeladat() throws InterruptedException {
        List<Thread> threadList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Thread t = new Thread() {
                @Override
                public void run() {
                    System.out.println("Hello");
                }
            };

            threadList.add(t);
            // Makes thread t available for the scheduler.
            t.start();
        }

        // With this we guarantee every thread to finish. Join waits till t. finishes! Great thing!
        for (Thread t : threadList) {
            t.join();
        }

        System.out.printf("A program lefutott.");
    }
}
