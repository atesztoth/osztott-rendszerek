package otodik;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by atesztoth on 2017. 03. 22..
 */
public class Server {

    private int port;
    private ServerSocket serverSocket;

    public static void main(String[] args) {
        Server server = null;

        try {
            server = new Server(11234);
        } catch (IOException e) {
            System.out.printf(e.getMessage());
            e.printStackTrace();
        }

        try {
            server.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Server(int port) throws IOException {
        this.port = port;
        this.serverSocket = new ServerSocket(port);
    }

    public void close() throws IOException {
        serverSocket.close();
    }


}
