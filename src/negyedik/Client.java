package negyedik;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by atesztoth on 2017. 03. 08..
 */
public class Client {

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost", Server.port);
        PrintWriter pw = new PrintWriter(socket.getOutputStream(), true);
        Scanner sc = new Scanner(socket.getInputStream());
        Random rand = new Random(); // netInt(x) [0,x)

        while (true) {
            int x = rand.nextInt(100); // [0,99)
            pw.println(x); // With autoflush turned on we are sending data...
            if (x == 0) {
                break;
            }
            System.out.println(sc.nextLine()); // And reading data that is coming back.
        }
    }

}
