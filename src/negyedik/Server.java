package negyedik;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by atesztoth on 2017. 03. 08..
 */
public class Server {

    // Órai megoldás !!
    private static int n = 0;

    static int port = 11234; // package private!

    public static void main(String[] args) {

        ServerSocket serverSocket = null;
        Socket client = null; // Getting connection from the client here.

        try {
            serverSocket = new ServerSocket(port);
            client = serverSocket.accept();
        } catch (IOException e) {
            System.out.printf("Full exceptionok.");
            e.printStackTrace();
        }

        Handler handler = new Handler(client);
        handler.run();
    }
}

class Handler extends Thread {
    private Socket client;
    private int n = 100;

    public Handler(Socket socket) {
        client = socket;
    }

    @Override
    public void run() {
        try {
            PrintWriter pw = new PrintWriter(client.getOutputStream(), true);
            Scanner sc = new Scanner(client.getInputStream());

            while (true) {
                // This way every client gets a new thread, independent from each other! YAY
                synchronized (Handler.class) {
                    int x = Integer.parseInt(sc.nextLine());
                    sendMessage(pw, sc, x);

                    if (x == 0) {
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(PrintWriter pw, Scanner sc, int x) {
        n += x;

        System.out.println("A küldött érték: " + x + ", új érték: " + n);

        pw.println(n);
    }
}
